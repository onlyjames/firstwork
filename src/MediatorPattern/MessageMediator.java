package MediatorPattern;

import java.util.*;

public class MessageMediator implements Mediator {
	private static List<VIPUser> vipList = new ArrayList<>();
	private static List<CommonUser> userList = new ArrayList<>();

	public static void joinChat(Messager messager) {
		if (messager.getClass().getSimpleName().equals("VIPUser")) {
			vipList.add((VIPUser) messager);
		} else {
			userList.add((CommonUser) messager);
		}
	}

	@Override
	// 發訊息給某人
	public void send(String message, String from, Messager to) {
		if (vipList.contains(to) || userList.contains(to)) {
			for (Messager msg : vipList) {
				if (msg.getName().equals(from)) {
					System.out.println(from + "->" + to.getName() + "：" + message);
				}
			}

			for (Messager msg : userList) {
				if (msg.getName().equals(from)) {
					System.out.println(from + "->" + to.getName() + "：" + message);
				}
			}
		}
	}

	@Override
	// 發送訊息給所有人
	public void sendToAll(String from, String message) {
		boolean isExist = false;
		for (Messager msg : vipList) {
			if (msg.getName().equals(from)) {
				isExist = true;
			}
		}
		if (isExist) {
			for (Messager msg : vipList) {
				if (!msg.getName().equals(from)) {
					System.out.println(from + "->" + msg.getName() + " ：" + message);
				}
			}
			for (Messager msg : userList) {
				if (!msg.getName().equals(from)) {
					System.out.println(from + "->" + msg.getName() + "：" + message);
				}
			}
		}
	}
}
