package MediatorPattern;
//目的：當有多個物件之間有交互作用，使用一個中介物件來負責這些物件的交互
public class Main {
	public static void main(String[] argu) {
		System.out.println("============中介者模式測試============");

		Messager jacky = new VIPUser("jacky");
		Messager huant = new CommonUser("huant");
		Messager neil = new CommonUser("neil");

		MessageMediator.joinChat(jacky);
		MessageMediator.joinChat(huant);
		MessageMediator.joinChat(neil);
		System.out.println("---VIP會員直接送訊息給每個人---");
		jacky.sendToAll("hi, 你好");

		System.out.println("---私底下送訊息---");
		jacky.send("hi!", huant);

		neil.send("收假了!!掰掰", jacky);
		System.out.println("當非VIP會員想送訊息給每個人");
		neil.sendToAll("阿阿阿!!!");
	}
}
