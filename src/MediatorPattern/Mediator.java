package MediatorPattern;

public interface Mediator {
//發訊息給某人
	public void send(String message,String from,Messager to);
	
	//發訊息給每個人
	public void sendToAll(String from,String message);
}
