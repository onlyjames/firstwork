package ObserverPatternBuildInJava;

import java.util.Observable;

public class WeatherData extends Observable {
	private float temperature;
	private float humidity;
	private float pressure;

	public WeatherData() {
		// TODO Auto-generated constructor stub
	}

	public void measurementsChanged() {
		setChanged(); // 呼叫setChanged 紀錄狀態已經改變
		notifyObservers(); // 出現更新 通知 觀察者
	}

	public void setMeasurements(float temperature, float humidity, float pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}

	public float getTemperature() {
		return temperature;
	}

	public float getHumidity() {
		return humidity;
	}

	public float getPressure() {
		return pressure;
	}
}
