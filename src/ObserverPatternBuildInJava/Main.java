package ObserverPatternBuildInJava;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] argu) {
		WeatherData ww = new WeatherData();
		CurrentConditionsDisplay cc = new CurrentConditionsDisplay(ww);
		ww.setMeasurements(1.2f, 25.3f, 89.3f);
		ww.setMeasurements(2.6f, 5.3f, 8.3f);

	}
}
