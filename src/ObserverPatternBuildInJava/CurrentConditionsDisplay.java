package ObserverPatternBuildInJava;
import java.util.Observable;
import java.util.Observer;


public class CurrentConditionsDisplay implements Observer {
	private float temperature;
	private float humidity;
	Observable OB;

	public CurrentConditionsDisplay(Observable in) {
		// TODO Auto-generated constructor stub
		this.OB=in;
		OB.addObserver(this);
	}

	
	public void update(Observable obs,Object arg) {
	if(obs instanceof WeatherData){
		WeatherData WD=(WeatherData)obs;
		this.temperature =WD.getTemperature();
		this.humidity = WD.getHumidity();
		display();
		}
	}

	public void display() {
		System.out.println("Current conditions : " + temperature + " " + humidity);
	}
}
