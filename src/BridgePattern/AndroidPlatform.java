package BridgePattern;

class AndroidPlatform implements Platform {
	public void control() {
		System.out.println("on Gingerbread");
	}
}