package BridgePattern;

interface Platform {
	void control();
}
