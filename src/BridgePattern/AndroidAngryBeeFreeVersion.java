package BridgePattern;

class AndroidAngryBeeFreeVersion extends AngryBee {
	 
	 public AndroidAngryBeeFreeVersion(Platform platform) {
	  super(platform);
	 }
	 
	 @Override
	 public void play() {
	  System.out.print("Playing AngryBee ");
	  platform.control();
	 }
	 
	}
