package BridgePattern;

class AndroidAngryBeeXmasVersion extends AndroidAngryBeeFreeVersion {  //在聖誕節下特別推出的版本 所以要往下繼承
	 
	 public AndroidAngryBeeXmasVersion(Platform platform) {
	  super(platform);
	 }
	 
	 @Override
	 public void play() {
	  System.out.print("Playing AngryBee Xmas ");
	  platform.control();
	 }
	 
	}
