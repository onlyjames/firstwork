package BridgePattern;

public abstract class AngryBee {
	 protected Platform platform;
	 
	 public AngryBee(Platform platform) {
	  this.platform = platform;
	 }
	 
	 public abstract void play();
	}