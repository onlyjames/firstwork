package VisitorPattern;



//黑暗料理界的廚師
public class DarkChef extends Chef {
	public DarkChef(String name) {
		super(name);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.cook(this);
	}
}
