package VisitorPattern;

import VisitorPattern.DarkChef;
import VisitorPattern.SuperChef;
import VisitorPattern.SuperNoodleChef;

//指定菜餚 拜訪者
public interface Visitor {
	// 利用Overlode 來實現每種不同的廚師煮出不同的指定菜餚
	void cook(DarkChef superChef);

	void cook(SuperChef superChef);

	void cook(SuperNoodleChef superNoodleChef);
}
