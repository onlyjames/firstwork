package VisitorPattern;



public abstract class Chef {
	private String name;

	public Chef(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	// visitor 代表裁判指定的料理
	public abstract void accept(Visitor visitor);
}
