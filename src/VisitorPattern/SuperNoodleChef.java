package VisitorPattern;



public class SuperNoodleChef extends Chef {
	public SuperNoodleChef(String name) {
		super(name);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.cook(this);
	}
}
