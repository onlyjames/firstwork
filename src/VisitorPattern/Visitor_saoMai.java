package VisitorPattern;



/**
 * ���w���N��(Concrete Visitor)
 */
public class Visitor_saoMai implements Visitor {

    @Override
    public void cook(DarkChef chef) {
        System.out.println(chef.getName() + " : �]�۾~���N��");
    }

    @Override
    public void cook(SuperChef chef) {
        System.out.println(chef.getName() + " : �t�z�j�N��");
    }

    @Override
    public void cook(SuperNoodleChef chef) {
        System.out.println(chef.getName() + " : �K��50�H���N��");
    }

}
