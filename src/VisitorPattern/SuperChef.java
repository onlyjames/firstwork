package VisitorPattern;



public class SuperChef extends Chef{
	public SuperChef(String name){
		super(name);
	}
//如何實現料理交給Visitor
	@Override
	public void accept(Visitor visitor) {
		// TODO Auto-generated method stub
		visitor.cook(this);
	}

}
