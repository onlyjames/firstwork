package IteratorPattern;
/*目的：提供方法走訪集合內的物件，走訪過程不需知道集合內部的結構*/
public class Main {
	public static void main(String[] args) {
		ConcreteAggregate collection = new ConcreteAggregate();
		collection.appendItem(new Person("Frank"));
		collection.appendItem(new Person("Jeny"));
		collection.appendItem(new Person("James"));
		Iterator it = collection.iterator();
		while (it.hasNext()) {
			Person person = (Person) it.next();
			System.out.println("" + person.getName());
		}
	}
}