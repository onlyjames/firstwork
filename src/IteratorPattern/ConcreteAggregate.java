package IteratorPattern;

import java.util.ArrayList;

public class ConcreteAggregate implements Aggregate {
	private ArrayList<Object> collection;
	// private VectorSample collection;
	private int last = 0;

	public ConcreteAggregate() {
		// collection = new VectorSample(3);
		collection = new ArrayList<Object>();
	}

	public Object getItemAt(int index) {
		return collection.get(index);
		// return ((Object)collection.get(index));
	}

	public void appendItem(Object item) {
		this.collection.add(item);
		last++;
		// collection.add(item);
	}

	public int getLength() {
		return last;
		// return collection.capacity();
	}

	public Iterator iterator() {
		return new ConcreteIterator(this);
	}
}