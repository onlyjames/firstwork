package PrototypeFlyweightCar;

public class Main {
	public static void main(String[] argu) {
		init();
		ProtoMgr protoMgr = ProtoMgr.getInstance();
		Car myFord = (Car) protoMgr.get("Ford");
		if (myFord == null) {
			Car car = new Car();
			car.setBrand("Ford");
			car.setEngine("V3");
			protoMgr.add("Ford", car);
		} else {
			Car c=(Car)myFord.clone();
			c.setEngine("V4");
			c.setName("James");
			System.out.println(c.getBrand());
			System.out.println(c.getEngine());
		}
	
	}

	static void init() {
		Car car = new Car();
		car.setBrand("Ford");
		
		ProtoMgr protoMgr = ProtoMgr.getInstance();
		protoMgr.add("Ford", car);

	}
}
