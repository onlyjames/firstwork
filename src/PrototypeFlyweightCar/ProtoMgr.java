package PrototypeFlyweightCar;

import java.util.HashMap;

public class ProtoMgr {
	private static ProtoMgr instance = new ProtoMgr();
	private HashMap<String, Prototype> pool = new HashMap<String, Prototype>();

	public static ProtoMgr getInstance() {
		return instance;
	}

	public void add(String n, Prototype in) {
		pool.put(n, in);
	}

	public Object get(String n) {
		return pool.get(n);
	}
}
