package PrototypeFlyweightCar;

public interface Prototype {
	public Object clone();
}
