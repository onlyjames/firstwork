package CommandPattern;

/*將請求封裝成物件，可以讓我們使用不同的請求、佇列、或者日誌來參數化其他物件。*/
/*引用GiveMepass 
 * 假設現在有三個按鈕, 分別處理三件不相同的事情, 
你會怎麼寫?
if(id == event1){
    //處理事件一
} else if(id == event2){
   //處理事件二
} else if(id == event3){
   //處理事件三
}
看起來似乎很直覺對吧? 
但是想想未來越來越多事件, 
我們的if…else就越來越長, 
這樣的設計看起來就是很糟糕~""~ 
另外萬一中間修改一些邏輯判斷, 
整段程式碼可能造成邊際效應, 進而產生負面影響。
這邊可以看到處理事件跟事件本身是緊緊綁住的, 
所以造成只要發生變化, 則必須整塊程式碼進行大規模的變動, 
產生bug的機率就大幅提升。
要改變這種情況, 則必須把事件跟事件處理鬆綁 */
public class Main {
	public static void main(String[] argu) {
		RemoteControl remoteControl = new RemoteControl();
		// ------- 設定按鈕區
		Light ll = new Light("Living room");
		LightOnCommand livingRoomLightOn = new LightOnCommand(ll);
		LightOffCommand livingRoomLightOff = new LightOffCommand(ll);

		CeilingFan CF = new CeilingFan("Living room");
		CeilingFanHighCommand HI = new CeilingFanHighCommand(CF);
		CeilingFanLowCommand LO = new CeilingFanLowCommand(CF);

		Command[] allON = { livingRoomLightOn, HI };
		Command[] allOFF = { livingRoomLightOff, LO };
		MacroCommand OnMacroCommand = new MacroCommand(allON);
		MacroCommand OffMacroCommand = new MacroCommand(allOFF);

		remoteControl.setCommand(0, livingRoomLightOn, livingRoomLightOff);
		remoteControl.setCommand(1, HI, LO);

		remoteControl.setCommand(2, OnMacroCommand, OffMacroCommand);
		// -----遙控器執行區
		System.out.println("-----普通按鈕-------");
		remoteControl.onButtonWasPushed(0); // 按下開按鈕 0
		remoteControl.offButtonWasPushed(0); // 按下關按鈕 0
		System.out.println("-----undo按鈕-------");
		remoteControl.undoButtonWasPushed(); // 回覆上一個動作 undo
		System.out.println("-----巨集按鈕-------");
		remoteControl.onButtonWasPushed(2); // 按下巨集指令
		remoteControl.undoButtonWasPushed(); // 巨集的undo
		System.out.println(remoteControl.toString());
	}
}
