package CommandPattern;

public class Light {
	public String lo;

	Light(String ll) {
		lo = ll;
	}

	public void on() {
		System.out.println(lo + "  Light on");
	}

	public void off() {
		System.out.println(lo + "  Light off");
	}
}
