package CommandPattern;

public class RemoteControl { // 遙控器
	private Command[] onCommands;
	private Command[] offCommands;
	private Command undoCommand;

	public RemoteControl() {
		onCommands = new Command[7]; // 遙控器上面有 7個 開的按鈕
		offCommands = new Command[7]; // 遙控器上面有7個關的按鈕
		Command noCommand = new NoCommand(); // 預設所有的按鈕 都沒有功能
		for (int i = 0; i < 7; i++) {
			onCommands[i] = noCommand;
			offCommands[i] = noCommand;
		}
		undoCommand = noCommand; // 一開始返回鍵 並沒有值
	}

	public void setCommand(int slot, Command onCommand, Command offCommand) { // 設定
		onCommands[slot] = onCommand;// 開按鈕
		offCommands[slot] = offCommand; // 關按鈕 // 要做什麼command
	}

	public void onButtonWasPushed(int slot) {
		onCommands[slot].execute();
		undoCommand = onCommands[slot];
	}

	public void offButtonWasPushed(int slot) {
		offCommands[slot].execute();
		undoCommand = offCommands[slot];
	}

	public void undoButtonWasPushed() {
		undoCommand.undo();
	}

	public String toString() {
		StringBuffer stringBuff = new StringBuffer();
		stringBuff.append("\n----- Remote Control -----\n");
		for (int i = 0; i < onCommands.length; i++) {
			stringBuff.append("[slot " + i + "] " + onCommands[i].getClass().getName() + "   "
					+ offCommands[i].getClass().getName() + "\n");
		}
		return stringBuff.toString();
	}
}
