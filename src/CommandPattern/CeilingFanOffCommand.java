package CommandPattern;

public class CeilingFanOffCommand implements Command {
	CeilingFan CF;
	int preSpeed;

	public CeilingFanOffCommand(CeilingFan ceilingFan) {
		this.CF = ceilingFan;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		preSpeed = CF.getSpeed();
		CF.off();

	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
		if (preSpeed == CeilingFan.HIGH) {
			CF.high();
		} else if (preSpeed == CeilingFan.MEDIUM) {
			CF.medium();
		} else if (preSpeed == CeilingFan.LOW) {
			CF.low();
		} else if (preSpeed == CeilingFan.OFF) {
			CF.off();
		}
	}
}
