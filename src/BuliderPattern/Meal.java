package BuliderPattern;

import java.util.*;

public class Meal {
	private ArrayList<Item> items = new ArrayList<Item>();

	public void addItem(Item item) {
		items.add(item);
	}

	public float getCost() {
		float cost = (float) 0.0;
		for (Item item : items) {
			cost += item.price();
		}
		return cost;
	}

	public void showItem() {
		for (Item item : items) {
		System.out.println("Item�G"+item.name());
		System.out.println("Packing�G"+item.packing().pack());
		System.out.println("Price�G"+item.price());
		}
	}
}
