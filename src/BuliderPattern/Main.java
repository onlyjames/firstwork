 package BuliderPattern;

public class Main {
	Meal ch=null;
	MealBulider MB = new MealBulider();
	public void construct(){
		ch=MB.prepareChickenMeal();
	}
	public Object ge0tProduct(){
		return ch;
	}
	public static void main(String[] argu) {
		// 點餐瞜
		MealBulider MB = new MealBulider(); // 建造者
		Meal ch = MB.prepareChickenMeal(); // 不會看到裡面的東西 只會知道他會建造一個 Meal
		ch.showItem();
		System.out.println();
		Meal veg = MB.prepareVegMeal();
		veg.showItem();
	}
}
/*
 * 1) Builder著重在隱藏複雜的建置步驟，最後只傳回一個產品。
 * 
 * 2) Abstract Factory則是為了維護一系列產品的關聯，會產出某系列的多項產品。
 * 
 * 3) Builder模式中，Client不需要認識各個零件的型態。（只要『吃』產出的餐點）
 * 
 * 4) Abstract Factory中，Client認識各項的抽象型別或介面，並能使用它們。
 
*/