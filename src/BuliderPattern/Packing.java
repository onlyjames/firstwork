package BuliderPattern;

public interface Packing {
    public String pack();
}
