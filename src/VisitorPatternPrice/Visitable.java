package VisitorPatternPrice;

public interface Visitable {
	public double accept(Visitor visitor);
}
