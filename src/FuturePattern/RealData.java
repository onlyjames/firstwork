package FuturePattern;

public class RealData implements MyData {
	private final String content;

	public RealData(final int count, final char c) {
		System.out.println("Making ReadData(" + c + ")BEGIN");
		char[] buffer = new char[count];
		for (int i = 0; i < count; i++) {
			buffer[i] = c;
			try {
				Thread.sleep(100);
			} catch (Exception e) {
			}
		}
		System.out.println("Making RealData " + count + ",END");
		this.content = new String(buffer);
	}

	@Override
	public String getContent() {
		return this.content;
	}

}
