package FuturePattern;

public class Host {
	public MyData request(final int count, final char c) {
		System.out.println("Request�G" + c + " BEGIN");
		// create FutureData object
		final FutureData future = new FutureData();
		// after create RealData , active new Thread
		new Thread() {
			public void run() {
				RealData realData = new RealData(count, c);
				future.setRealData(realData);
			}
		}.start();
		System.out.println("Request�G" + c + " END");
		// return futureData object
		return future;
	}
}
