package FuturePattern;

public class FutureData implements MyData {
	private RealData realData = null;
	private boolean ready = false;

	public synchronized void setRealData(RealData realData) {
		if (ready) {
			return;// break
		}
		this.realData = realData;
		this.ready = true;
		// wake up main thread
		notifyAll();
	}

	@Override
	public synchronized String getContent() {
		while (!ready) {
			try {
				wait();
			} catch (Exception e) {

			}
		}
		return realData.getContent();
	}

}
