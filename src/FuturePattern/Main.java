package FuturePattern;

public class Main {

	public static void main(String[] args) {
		Host host = new Host();
		MyData data1 = host.request(20, 'J');
		System.out.println(data1.getContent());

	}

}
