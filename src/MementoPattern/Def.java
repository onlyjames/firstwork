package MementoPattern;

public class Def implements Cloneable {

	private int def = 100;

	public void setDef(int in) {
		def = in;
	}

	public int getDef() {
		return def;
	}

	@Override
	public Object clone() {
		Object ob = null;
		try {
			ob = super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ob;
	}
}
