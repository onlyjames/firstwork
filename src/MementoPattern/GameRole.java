package MementoPattern;

public class GameRole {   //Originator can have memento. Originator can do redo and undo.
	private HP hp = null;
	private Attack atk = null;
	private Def def = null;
	private String name = "魔王";

	GameRole() {
		hp = new HP();
		atk = new Attack();
		def = new Def();
	}

	public RoleStateMemo save() {
		RoleStateMemo role= new RoleStateMemo(this.hashCode());
		//(this.hashCode(), hp.clone(), atk.clone(), def.clone());
		role.setAddData("hp", hp.clone());
		role.setAddData("attack", atk.clone());
		role.setAddData("def", def.clone());
		return  role;
	}
		

	/**
	 * 
	 */
	public void fight() {
		hp.setHP(30);
		System.out.println(name + "剩下30%血量，出大招把隊伍打的半死");
	}

	public void stateDisplay() {
		System.out.println(name + "的狀態：");
		System.out.print("hp=" + hp.getHP());
		System.out.print(", atk=" + atk.getAttack());
		System.out.println(", def=" + def.getDef());
		System.out.println();
	}

	public void load(RoleStateMemo memo) {
		this.hp = (HP) memo.getData(this.hashCode(),"hp");
		this.atk = (Attack) memo.getData(this.hashCode(),"attack");
		this.def = (Def) memo.getData(this.hashCode(),"def");
	}

	public void setAtk(int atk) {
		this.atk.setAttack(atk);
	}
}
