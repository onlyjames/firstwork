package MementoPattern;

//目的：將一個物件的內部狀態儲存在另外一個備忘錄物件中，備忘錄物件可用來還原物件狀態
public class Main {
	public static void main(String[] args) {
		// boss一開始的狀態
		GameRole boss = new GameRole();
		boss.stateDisplay();

		// 使用複雜的神秘小技巧，可以降低boss攻擊力
		System.out.println("使用複雜的神秘小技巧");
		boss.setAtk(60);

		// 趕快存檔
		RoleStateCareTaker rsc = new RoleStateCareTaker();
		rsc.setSave(boss.save());
		boss.stateDisplay();

		// 開打了
		boss.fight();
		boss.stateDisplay();

		// 隊伍血沒先回滿，倒了一半，快讀取剛才的存檔
		boss.load(rsc.getSave());
		System.out.println("不行不行，那個時間點先該先回滿血，讀檔重打");
		boss.stateDisplay();
	}
}/*
	 * 下面我們用打魔王小遊戲來模擬，在戰鬥前有個複雜神秘的密技可以降低魔王的攻擊力，不過因為太複雜了，
	 * 所以使用後我們就先使用備忘錄物件(Memento)將魔王的狀態儲存，當戰鬥不順利需要重來的時候我們可以使用Memento將
	 * 魔王的狀態恢復到開打之前。
	 */
