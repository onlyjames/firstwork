package MementoPattern;

import java.util.HashMap;

/**
 * 備忘錄物件(Memento)
 */
public class RoleStateMemo {
	private HashMap<String, Object> state = new HashMap<String, Object>();
	private int id = 0;

	public RoleStateMemo(int id) {
		this.id=id;
	}
	public void setAddData(String in,Object data){   //儲存資料
		state.put(in, data);
	}
	

	public Object getData(int id,String dataType){
		if(this.id==id){
			return state.get(dataType);
		}
		return null;
	}
	
}
