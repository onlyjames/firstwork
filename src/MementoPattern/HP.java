package MementoPattern;

public class HP  implements Cloneable{
	private int hp=100;
	public int getHP(){
		return hp;
	}
	public void setHP(int in){
		hp=in;
	}
	@Override
	public Object clone() {
		Object ob=null;
		try {
			ob= super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ob;
	}
}
