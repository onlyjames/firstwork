package CommandAndTemplatePattern;

import java.util.Calendar;
import java.util.Date;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class UpdateBook extends UpdateCommand {
	@Override
	public Object processResult() {
		Book BK = (Book) super.getObject();   //呼叫父類別方法
		try {
			//conn = (Connection) dbMgr.initDB();   //使用父類別屬性
			Calendar c = Calendar.getInstance();
			c.setTime(BK.GetDueDay());
			c.add(Calendar.DAY_OF_YEAR, GlobalVariable.u.getDueDay());// //續借後日期
			// Dynamic binding會根據綁定的instance來決定要執行那個DueDay方法
			String sql = "UPDATE UserBook SET DueDay = ?  WHERE UId=?  AND BId=? ;";
			pst = (PreparedStatement) conn.prepareStatement(sql);
			pst.setString(1, (String) DelDate.delDateString(c.getTime().toString()));
			pst.setString(2, (String) GlobalVariable.u.getUid()); //使用在GlobalVariable裡面的User來取得 uid
			pst.setString(3, BK.GetBid());
			pst.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
