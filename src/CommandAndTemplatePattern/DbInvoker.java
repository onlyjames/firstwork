package CommandAndTemplatePattern;

public class DbInvoker {
	private SqlCmd sqlCmd = null;

	public void readUser(String Uid) {
		sqlCmd = new ReadUser(Uid);
		sqlCmd.execute();
	}

	public BookList readBook(String Uid) {
		sqlCmd = new ReadBook(Uid);
		sqlCmd.execute();
		return (BookList) sqlCmd.getObject();
	}

	public void updateBook(Book bk) {
		sqlCmd = new UpdateBook();
		sqlCmd.setObject(bk);
		sqlCmd.execute();
	}
}
