package CommandAndTemplatePattern;

import java.sql.ResultSet;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

//不能跑  範例程式碼
public abstract class SqlCmd {

	String name;
	DbMgr dbMgr;
	ResultSet resultSet;
	Connection conn = null;
	Statement sta = null;
	ResultSet rs = null;
	PreparedStatement pst = null;
	Object object;

	public SqlCmd() {
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public boolean execute() {
		try {
			openDB(); // LOD 自己呼叫自己的方法
			queryDB(); // LOD 自己呼叫自己的方法
			object = processResult(); // LOD 自己呼叫自己的方法 掛勾的概念
			disconnectDB(); // LOD 自己呼叫自己的方法
			return true; // 確認執行成功

		} catch (Exception e) {
			// disconnectDB();

			e.printStackTrace();
		}
		return false;
	}

	public abstract Object processResult();

	public abstract void disconnectDB();
	// TODO Auto-generated method stub
	// Close
	// dbMgr.closeDBBatch(pst, conn);
	// dbMgr.closeDB(sta, conn);

	public void openDB() { // 功能性method cohesion 只做 開啟資料庫的連結
		try {
			dbMgr = DbMgr.getDbInstance(); // 這邊會拿到Insatnce
			conn = (Connection) dbMgr.initDB(); // 取得資料庫連結
		} catch (Exception sqle) {
			sqle.printStackTrace();
		}
	}

	public abstract void queryDB();

}
