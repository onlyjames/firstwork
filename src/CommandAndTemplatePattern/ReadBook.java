package CommandAndTemplatePattern;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;

public class ReadBook extends ReadCommand{
	private String uid;

	public ReadBook(String u) {
		this.uid = u;
	}

	@Override
	public Object processResult() {
		// TODO Auto-generated method stub
		ArrayList<Book> ALB = new ArrayList<Book>();
		try {
			while (rs.next()) {
				Book b = new Book(rs.getString("BName"), rs.getString("BId"), rs.getString("Reserve"),
						rs.getDate("DueDay"));
				ALB.add(b);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// dbMgr.closeDB(rs, sta, conn);
		}
		return new BookList(ALB);
		
	}

	@Override
	public void queryDB() {
		// TODO Auto-generated method stub

		try {
			
			String sql = "SELECT * FROM userbook WHERE UId=?";
			pst = (com.mysql.jdbc.PreparedStatement) conn.prepareStatement(sql);
			pst.setString(1, uid);
			pst.execute();
			rs = pst.getResultSet();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * public static void main(String[] argu) { SqlCmd sqlCmd = new
	 * ReadUser("B10323023"); sqlCmd.execute(); }
	 */
}


