package CommandAndTemplatePattern;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbMgr {
	// 屬性沒有 其他領域或同層的物件
	// 而這個class只做 DB的連結 所以沒有混和型的cohesion
	private static DbMgr db;

	private DbMgr() {
	}

	public static void main(String[] argu) {
		DbMgr.getDbInstance().initDB();
	}

	public Connection initDB() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String datasource = "jdbc:mysql://localhost/library?user=root&password=123456";
			conn = (Connection) DriverManager.getConnection(datasource);
		} catch (Exception e) {
			System.out.println("Exception :" + e.toString());
		}
		return conn;
	}

	public static synchronized DbMgr getDbInstance() { // Singleton pattern
		if (db == null) {
			db = new DbMgr();
		}
		return db;
	}
	/*
	 * public void closeDB(Statement sta, Connection conn) { try {
	 * 
	 * sta.close(); conn.close(); } catch (SQLException e) {
	 * e.printStackTrace(); } }
	 */

	public void closeDB(ResultSet rs, PreparedStatement pst, Connection conn) {
		try {
			rs.close();
			pst.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void closeDBBatch(PreparedStatement pst, Connection conn) {
		try {
			pst.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}