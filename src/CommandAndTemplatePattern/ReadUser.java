package CommandAndTemplatePattern;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class ReadUser extends ReadCommand {
	private String uid;

	public ReadUser(String u) {
		this.uid = u;
	}

	@Override
	public Object processResult() {
		// TODO Auto-generated method stub

		try {
			if (rs.next()) {
				String u = rs.getString("UId");
				String p = rs.getString("Password");
				if (u.contains("B")) {                   //判斷使用者是老師(Txxxxx)還是學生(Bxxxxx)
					GlobalVariable.u = new student(u, p); // 這邊根據User的不同(student
															// or teacher)會給不同的Instance 綁定
				} else {
					GlobalVariable.u = new teacher(u, p);
				}
				// System.out.println(rs.getString("UId") + " " +
				// rs.getString("Password"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// dbMgr.closeDB(rs, sta, conn);
		}

		return null;
	}

	@Override
	public void queryDB() {
		// TODO Auto-generated method stub

		try {
			conn = (Connection) dbMgr.initDB();
			String sql = "SELECT * FROM member WHERE UId=?";
			pst = (com.mysql.jdbc.PreparedStatement) conn.prepareStatement(sql);
			pst.setString(1, uid);
			pst.execute();
			rs = pst.getResultSet();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * public static void main(String[] argu) { SqlCmd sqlCmd = new
	 * ReadUser("B10323023"); sqlCmd.execute(); }
	 */
}
