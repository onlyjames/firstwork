package StateCruising;
public class Cruising extends CruiseActivated {
	@Override
	void leverDown(CruiseControl c) {
		// TODO Auto-generated method stub
		System.out.println("Cruising change to Cruising");
		setDesiredSpeed(c);
		c.setState(new Cruising());
	}

	@Override
	void brakeApplied(CruiseControl c) {
		// TODO Auto-generated method stub
		System.out.println("Cruising change to CruisingCancelled");
		c.setState(new CruisingCancelled());
	}

	@Override
	void leverDownAndHold(CruiseControl c) {
		// TODO Auto-generated method stub
		System.out.println("Cruising change to DecreasingSpeed");
		c.setState(new DecreasingSpeed());
	}

	@Override
	void leverUpAndHold(CruiseControl c) {
		// TODO Auto-generated method stub
		System.out.println("Cruising change to IncreasingSpeed");
		c.setState(new IncreasingSpeed());
	}

	@Override
	void leverPulled(CruiseControl c) {
		// TODO Auto-generated method stub
		System.out.println("Cruising change to CruisingCancelled");
		c.setState(new CruisingCancelled());
	}
}
