package StateCruising;
public class DecreasingSpeed extends CruiseActivated{
	void leverReleased(CruiseControl c){
		System.out.println("DecreasingSpeed change to Cruising");
		setDesiredSpeed(c);
		c.setState(new Cruising());
	}
}
