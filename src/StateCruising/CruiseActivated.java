package StateCruising;


public class CruiseActivated extends State {

	@Override
	void onOffButtonPressed(CruiseControl c) {
		// TODO Auto-generated method stub
		System.out.println("CruiseActivated change to CruiseDeactivated");
		c.setState(new CruiseDeactivated());
	}

	@Override
	void leverDown(CruiseControl c) {
		// TODO Auto-generated method stub

	}

	@Override
	void leverUp(CruiseControl c) {
		// TODO Auto-generated method stub

	}

	@Override
	void brakeApplied(CruiseControl c) {
		// TODO Auto-generated method stub

	}

	@Override
	void leverDownAndHold(CruiseControl c) {
		// TODO Auto-generated method stub

	}

	@Override
	void leverUpAndHold(CruiseControl c) {
		// TODO Auto-generated method stub

	}

	@Override
	void leverPulled(CruiseControl c) {
		// TODO Auto-generated method stub

	}

	@Override
	void leverReleased(CruiseControl c) {
		// TODO Auto-generated method stub

	}
}
