package DecoratorStreamPattern;

import java.util.ArrayList;

public abstract class Stream {
	static ArrayList<Integer> integer = new ArrayList<Integer>();
	static ArrayList<String> string = new ArrayList<String>();

	public Stream() {

	}

	public void PutInt(int in) {
		integer.add(in);
	}

	public void PutString(String in) {
		string.add(in);
	}

	public abstract void HandleBufferFull();
}
