package DecoratorStreamPattern;

public abstract class StreamDecorator extends Stream {
	Stream stream = null;

	StreamDecorator(Stream stream) {
		this.stream = stream;
	}

	public void HandleBufferFull(){
		stream.HandleBufferFull();
	}
}
