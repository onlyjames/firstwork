package DecoratorStreamPattern;

public class Main {
	public static void main(String[] argu) {
		Stream test = new CompressingStream(new ASCII7Stream(new FileStream("TEST")));
		test.PutInt(12325465);
		test.PutString("James is the most handson in the word.");
		test.HandleBufferFull();
	}
}
