package DecoratorStreamPattern;

public class FileStream extends Stream {
	String name;

	FileStream(String name) {
		this.name = name;
	}

	@Override
	public void HandleBufferFull() {
		// TODO Auto-generated method stub

		if (integer.size() != 0 && string.size() != 0) {
			System.out.println("File has ");
			if (integer.size() != 0) {
				System.out.println("Integer:");
				integer.forEach(n -> System.out.print(n + " "));
				System.out.println();
			}
			if (string.size() != 0) {
				System.out.println("String:");
				string.forEach(n -> System.out.print(n + " "));
				System.out.println();
			}
		}else{
			System.out.println("File is Null");
		}
	}

}
