package FacadePattern;

/*目的：用一個介面包裝各個子系統，由介面與客戶端做溝通*/
public class Main {
	public static void main(String[] argu) {
		VedioRoomFacade superRemote = new VedioRoomFacade();
		System.out.println("============外觀模式測試============");
		System.out.println("以下測試碼可以看出使用外觀模式後，操作步驟會比一個一個類別進去操作方便取多");

		System.out.println("---看電影---");
		// 看電影
		superRemote.readyPlayMovie("Life of Pi");
		superRemote.playMovie();
		superRemote.showAllStatus();
		System.out.println();
		System.out.println("---關機器---");
		// 關閉機器
		superRemote.turnOffAll();
		superRemote.showAllStatus();

		System.out.println("---看電視---");
		// 看電視
		superRemote.watchTv();
		superRemote.showTv();
		superRemote.switchChannel(20); // 換頻道
		superRemote.showTv();
		superRemote.turnOffAll();
		System.out.println();

		System.out.println("---唱ktv---");
		// 唱ktv
		superRemote.readyKTV();
		superRemote.selectSong("Moon");
		superRemote.playSong();
		superRemote.showAllStatus();

	}
}
