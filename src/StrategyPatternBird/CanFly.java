package StrategyPatternBird;

public class CanFly implements Fly {
	public void fly() {
		System.out.println("Can fly.");
	}
}
