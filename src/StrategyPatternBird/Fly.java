package StrategyPatternBird;

public interface Fly {
	public void fly();
}