package StrategyPatternBird;


public class Main {
	public static void main(String[] argu) {
		Bird g1 = new Goose(new CanFly());
		Bird d1 = new Duck(new CantFly());
		g1.fly();
		d1.fly();
	}
}

























/*
 * 引用givemepass
 * 一開始使用繼承的方式去實作
 * 可是這樣會出現一個問題,就是程式靈活度不夠, 
想看看今天我有50個角色,裡面有20個角色攻擊都是揮舞劍, 
這樣我每個角色都要去覆寫fight()不就累死了
所以我們將fight()拿出來寫成介面(interface)
*/