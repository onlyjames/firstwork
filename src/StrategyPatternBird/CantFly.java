package StrategyPatternBird;

public class CantFly implements Fly{
	public void fly() {
		System.out.println("Can't fly.");
	}
}
