package ObservePattern;
/*
 * 有時候不知道你會不會出現一些疑問? 
當你為一些元件加入事件以後, 而這些元件產生變化的時候, 
為什麼系統會知道是哪一個元件產生變化的呢? 
其實他是由一種設計模式產生的, 這種設計模式稱作為”觀察者模式”
 * */
public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] argu) {
		WatherData ww = new WatherData();
		
		CurrentConditionsDisplay cc=new CurrentConditionsDisplay(ww);
		ww.setMeasurements(10.2f, 20.2f, 36.3f);
		cc.re();
		ww.setMeasurements(10.2f, 20.8f, 39.3f);
		//cc.re();
	}
}
