package AdapterPattern;

public class Main {
	static void testDuck(Duck duck) { // package
		duck.quack();
		duck.fly();
	}
//鴨子物件不夠  所以使用火雞物件來充當鴨子 
	public static void main(String[] argu) {
		MallardDuck duck = new MallardDuck(); // create duck and turkey object
		WildTurkey turkey = new WildTurkey();
		// let's turkey look like duck by use adapter.
		Duck turkeyAdapter = new TurkeyAdapter(turkey);
		System.out.println("The turkey says....");
		turkey.gobble();
		turkey.fly();
		System.out.println("\nThe Duck says....");
		testDuck(duck);
		System.out.println("\nThe TurkeyAdapter says....");
		testDuck(turkeyAdapter);
	}
}
