package AdapterPattern;

public class TurkeyAdapter implements Duck { // Duck's object isn't enough.So we
												// use turkey to replace
	// Use adapter to replace Duck object
	Turkey turkey;

	public TurkeyAdapter(Turkey turkey) {
		this.turkey = turkey;
	}

	@Override
	public void quack() {
		turkey.gobble();
	}

	@Override
	public void fly() {
		for (int i = 0; i < 5; i++) {
			turkey.fly(); // Because turkey fly short distance,we use call
							// turkey's fly method not just one to replace
							// Duck's fly method.
		}
	}
}
