package DecoratorPatternAbstractBeverageCoffee;

public abstract class Beverage {
	String description = "Unknow Beverage";

	public Beverage() {
		// TODO Auto-generated constructor stub
	}
	public String getDescription(){
		return description;
	}
	public abstract double cost();
}
