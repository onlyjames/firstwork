package DecoratorPatternAbstractBeverageCoffee;

public abstract class CondimentDecorator extends Beverage{
	Beverage be; // �Q�˹���(*****)  
	public CondimentDecorator( Beverage in) {
		// TODO Auto-generated constructor stub
	be=in;
	
	}
	public  String getDescription(){
		return be.getDescription();
	}
	public void removeDecorator(Beverage toRemove) {
		  if (be == null) {
		    return;
		  } else if (be.equals(toRemove)) {
			  be = ((CondimentDecorator)be).getSubject();
		  } else {
			  ((CondimentDecorator)be).removeDecorator(toRemove);
		  }
		}

		public Beverage getSubject() {
		  return be;
		}
}
