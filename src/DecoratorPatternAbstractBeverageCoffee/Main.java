package DecoratorPatternAbstractBeverageCoffee;

/*引用 https://skyyen999.gitbooks.io/-study-design-pattern-in-java/content/decorator.html
 * "風暴降生，不焚者，彌林女王，安達爾人，羅伊納人和先民的女王，七國統治者暨全境守護者、多斯拉克大草原的卡麗熙、碎鐐者、龍之母"，
 * 上面那個寫爽的
 *  冒險者可以透過各種冒險或訓練得到稱號加強本身的能力，例如說"強壯的冒險者"攻擊力比較高，"堅毅的冒險者"生命力比較高，
 *   "炎龍的冒險者"攻擊的時候可以順便用火燒敵人，在前面的設計中，我們可能新增三個繼承冒險者子類別來實現這樣的設計， 不過冒險者是可以取得很多稱號的，例如"強壯堅毅敏捷的冒險者"，"強壯飛翔的冒險者"
 *   等等各種交差排列組合，如果可以選的稱號有3種， 那我們就要建立321=6個子類別，如果可以選的稱號有5種，那要建立的子類別就多達120種。這還沒算上冒險者可以取得重複的稱號的情況， 
 *   例如"強壯的強壯的冒險者"，那要建立的子類別就更多了。
為了避免上面這種很可怕的事情發生，這邊可以使用裝飾者模式來處理*/














public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] argu) {
		Beverage t = new Espresso();
		System.out.println(t.getDescription() + " $ " + t.cost());
		Beverage t2 = new Mocha(t);
		
		System.out.println(t2.getDescription() + " $ " + t2.cost());
	}
}
