package SimpleFactoryPatternShape;

public abstract class Shape {   
    public abstract void draw();
}