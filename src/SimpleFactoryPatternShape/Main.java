package SimpleFactoryPatternShape;

/*
 * 當我們撰寫應用程式時，如果無法明確地知道要產生的類別，需使用那一種物件型態時，你應該考慮使用 Factory Method 模式，讓子類別去指定要生成物件的類型*/
public class Main {
	public static void main(String args[]) {
		Shape cirle=SimpleFactory.createProduct("circle");
		cirle.draw();
		SimpleFactory.createProduct("square").draw();
	}
}
