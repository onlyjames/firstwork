package SimpleFactoryPatternShape;

public class SimpleFactory {
    public static Shape createProduct(String product) {
        if(product.equals("circle")){
            return new Circle();
        }else if(product.equals("square")){
            return new Square();
        }else {
            System.out.println("�L�����~");
            return null;
        }
    }
}
