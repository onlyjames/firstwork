package AbstractFactoryPatternShape;

public interface Color {    
    public void fill();
}