package AbstractFactoryPatternShape;

public interface Shape {
    public void draw();
}