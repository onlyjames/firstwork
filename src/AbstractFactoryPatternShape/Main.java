package AbstractFactoryPatternShape;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] arge) {
		AbstractFactory shapeFactory = FactoryProducer.getFactory("SHAPE");  //拿到工廠
		Shape shape1 = shapeFactory.getShape("CIRCLE");    //叫工廠 做事     拿到產品
		shape1.draw();  //產品做事

		Shape shape3 = shapeFactory.getShape("SQUARE");
		shape3.draw();

		AbstractFactory colorFactory = FactoryProducer.getFactory("COLOR");
		Color color1 = colorFactory.getColor("RED");
		color1.fill();

		Color color2 = colorFactory.getColor("WHITE");
		color2.fill();
	}
}
