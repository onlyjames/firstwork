package CirclarReference;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class A implements Serializable, Cloneable {
	private B b = null;
	private static A instance = new A();

	public static A getInstance() {
		return instance;
	}

	A() {
		b = new B(this);
	}

	public A deepClone() {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(this);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return (A) ois.readObject();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("IOException");
			return null;
		} catch (ClassNotFoundException e) {
			System.out.println("ClassNotFoundException");
			return null;
		}
	}

	public static void main(String[] argu) {
		A k = A.getInstance();
	}
}
