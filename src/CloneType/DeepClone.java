package CloneType;

public class DeepClone implements Cloneable {

	private int a; // 基本類型
	private int[] b; // 非基本類型
	// overrid Object.clone()方法,並把protected改為public

	@Override
	public Object clone() {
		DeepClone sc = null;
		try {
			sc = (DeepClone) super.clone();
			int[] t = sc.getB();
			int[] b1 = new int[t.length];
			for (int i = 0; i < b1.length; i++) {
				b1[i] = t[i];
			}
			sc.setB(b1);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return sc;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int[] getB() {
		return b;
	}

	public void setB(int[] b) {
		this.b = b;
	}
	public static void main(String[] args) throws CloneNotSupportedException{
		DeepClone c1 = new DeepClone();
        //對c1賦值
        c1.setA(100) ;
        c1.setB(new int[]{1000}) ;
        
        System.out.println("clone前c1:  a="+c1.getA()+" b[0]="+c1.getB()[0]);
        //clone出對象c2,並對c2的屬性A,B,C進行修改
        DeepClone c2 = (DeepClone) c1.clone();
        //對c2進行修改
        System.out.println("clone後c1(c2):  a="+c2.getA()+" b[0]="+c2.getB()[0]);
        c2.setA(50) ;
        int[] a = c2.getB() ;  
        a[0]=5 ;
        c2.setB(a);  //對c2的b做更改 c1的b 不會跟著變動
        
        System.out.println("更改c2值:  a="+c2.getA()+ " b[0]="+c2.getB()[0]);
        System.out.println("c1的值:  a="+c1.getA()+" b[0]="+c1.getB()[0]);
	
	}
}