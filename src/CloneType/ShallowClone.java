package CloneType;

import java.util.ArrayList;

public class ShallowClone implements Cloneable {

	private int a; // 基本類型 會自己挖記憶體
	private int[] b; // 非基本類型 需要外部傳入 (new)
	public ArrayList<String> test = new ArrayList<String>();
	// override Object.clone()方法,並把protected改為public

	@Override
	public Object clone() {
		ShallowClone sc = null;
		try {
			sc = (ShallowClone) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return sc;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int[] getB() {
		return b;
	}

	public void setB(int[] b) {
		this.b = b;
	}

	public static void main(String[] args) throws CloneNotSupportedException {
		ShallowClone c1 = new ShallowClone();
		// 對c1賦值
		c1.setA(100);
		c1.setB(new int[] { 1000 });

		System.out.println("克隆前c1:  a=" + c1.getA() + " b=" + c1.getB()[0]);
		// clone出對象c2,並對c2的屬性A,B,C進行修改
		ShallowClone c2 = (ShallowClone) c1.clone();
		// 對c2進行修改
		c2.setA(50);
		int[] a = c2.getB();
		a[0] = 5;
		c2.setB(a); // 對c2的b做更改 c1的b 會跟著變動
		System.out.println("clone前c1:  a=" + c1.getA() + " b=" + c1.getB()[0] + "  hashCode " + c1.getB().hashCode());
		System.out
				.println("clone後c2:  a=" + c2.getA() + " b[0]=" + c2.getB()[0] + "  hashCode " + c2.getB().hashCode());
		c1.test.add("test");
		System.out.println(c2.test.get(0));
	}
}
