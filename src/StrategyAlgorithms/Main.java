package StrategyAlgorithms;

//this sample follow p397
public class Main {
	//Main is Client
	public static void main(String[] argu) {
		Context context = new Context();
		Strategy strategy = new Strategy1(); // custom can select strategy
		strategy.setContext(context);
		strategy.operation();
	}
}