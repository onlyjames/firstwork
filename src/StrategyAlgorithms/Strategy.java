package StrategyAlgorithms;

public abstract class Strategy {
	Context context = null;

	public void setContext(Context context) {
		this.context = context;
	}
	public abstract void operation();
}
