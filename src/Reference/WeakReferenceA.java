package Reference;

import java.lang.ref.WeakReference;

public class WeakReferenceA {
	int id = 0;

	public static void main(String[] argu) {
		/**
		 * When I new A() , the program will put this memory in heap. a will be store in
		 * stack.
		 **/
		WeakReferenceA a = new WeakReferenceA(); // strong reference
		/**
		 * a will be popped up when Main function exit.
		 **/

		/**
		 * When GC want to recycle a's memory , a must point to null. Like this. This
		 * thing violates GC police. Or program need cache. If these references exist in
		 * cache , these memory will be not recycled.
		 **/
		a = null;
		/** So we have weak reference **/
		WeakReferenceA a1 = new WeakReferenceA();
		WeakReference<WeakReferenceA> weakA = new WeakReference<WeakReferenceA>(a1);
		// When we use this, we must check that it is not null.
		while (true) {
			if (weakA.get() != null) {
				System.out.println(weakA.get().id);
			} else {
				System.out.println("A has been collected.");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}
}
