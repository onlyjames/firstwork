package Reference;

import java.lang.ref.SoftReference;

public class SoftReferenceA {
	int id = 0;

	public static void main(String[] args) {
		SoftReferenceA a = new SoftReferenceA();
		SoftReference<SoftReferenceA> softRef = new SoftReference<SoftReferenceA>(a);
		System.out.println(softRef.get().id);
	}
}
