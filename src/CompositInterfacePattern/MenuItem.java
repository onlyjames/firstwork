package CompositInterfacePattern;

public class MenuItem implements MenuComponent{  
    private String name;  
    private String description;  
    private boolean isVeget;  
    private double price;  
      
    public MenuItem(String name, String des, boolean isVeget, double price)  
    {  
        this.name = name;  
        this.description = des;  
        this.isVeget = isVeget;  
        this.price = price;  
    }  
  
    @Override  
    public String getName() {  
        return name;  
    }  
  
    @Override  
    public String getDescription() {  
        return description;  
    }  
  
    @Override  
    public boolean isVegetarian() {  
        return isVeget;  
    }  
  
    @Override  
    public double getPrice() {  
        return price;  
    }  
  
    @Override  
    public void print()  
    {  
        System.out.print("\t"+getName());  
        if(isVegetarian())  
        {  
            System.out.print("(v)");              
        }  
        System.out.println(", Price="+getPrice());  
        System.out.println("\t"+getDescription());  
    }  
}  