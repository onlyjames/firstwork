package CompositInterfacePattern;

/*
 * iterator 無法表示樹狀的菜單 所以使用Composite表示
 * */

//在這裡使用轉型
		//在Interface寫出全部的add remove 然後 由下面的class 實作時 丟出 unSupportedOperationException
		
public class Main {
	public static void main(String args[]) {
		MenuComponent pancakeHouseMenu = new Menu("PANCAKE HOUSE MENU", "Breakfast");
		MenuComponent dinerMenu = new Menu("DINER MENU", "Lunch");
		MenuComponent cafeMenu = new Menu("CAFE MENU", "Diner");
		MenuComponent dessertMenu = new Menu("DESSERT MENU", "Dessert of course!");

		MenuComponent allMenus = new Menu("ALL MENUS", "All menus combination");
		((Menu)allMenus).add(pancakeHouseMenu);
		((Menu)allMenus).add(dinerMenu);
		((Menu)allMenus).add(cafeMenu);

		((Menu)dinerMenu).add(new MenuItem("Pasta", "Spaghetti with...", true, 3.89));
		((Menu)dinerMenu).add(dessertMenu);

		((Menu)dessertMenu).add(new MenuItem("Apple Pie", "Apple pie with a flakey crust, topped with...", true, 1.59));
		//在這裡使用轉型
		//在Interface寫出全部的add remove 然後 由下面的class 實作時 丟出 unSupportedOperationException
		
		//allMenus.add(dinerMenu);
		// allMenus.add(dessertMenu);
		allMenus.print();

	}
}