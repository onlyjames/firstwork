package CompositInterfacePattern;

import java.util.ArrayList;
import java.util.Iterator;

public class Menu implements MenuComponent {
	private String name;
	private String description;
	private ArrayList<MenuComponent> menuComponents;

	public Menu(String name, String dest) {
		this.name = name;
		this.description = dest;
		menuComponents = new ArrayList();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	
	public void add(MenuComponent comp) {
		menuComponents.add(comp);
	}


	public void remove(MenuComponent comp) {
		menuComponents.remove(comp);
	}

	
	public MenuComponent getChild(int i) {
		return menuComponents.get(i);
	}

	
	public void print() {
		System.out.print("\n" + getName() + ", " + getDescription() + "\n");
		System.out.println("=====================================");

		for (MenuComponent comp : menuComponents) {
			comp.print();
		}
	}

	@Override
	public double getPrice() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isVegetarian() {
		// TODO Auto-generated method stub
		return false;
	}
}