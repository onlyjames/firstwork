package CompositInterfacePattern;

public interface MenuComponent {
    public String getName();
    public String getDescription();
    public double getPrice();  
    public boolean isVegetarian();  
    public void print();
}  