package FlyweightPattern;

public class Main {
	private static final String colors[] = { "Red", "Green", "Blue", "White", "Black" };

	public static void main(String[] args) {
		for (int i = 0; i < 5; ++i) {
			Circle circle = (Circle) ShapeFactory.getCircle(getRandomColor());
			circle.setX(getRandomX());
			circle.setY(getRandomY());
			circle.setRadius(100);
			circle.draw();
		}
	}

	/*
	 * 優點
	 * 
	 * 降低內存中對象的數量。 外部狀態獨立，可使享元對象在不同環境被共享。
	 * 
	 * 缺點
	 * 
	 * 需要分離出內部狀態和外部狀態，使得系統變複雜。 為了使對象可以共享，享元模式需要將享元對象的狀態外部化，而讀取外部狀態使運行時間變長。
	 */
	private static String getRandomColor() {
		return colors[(int) (Math.random() * colors.length)];
	}

	private static int getRandomX() {
		return (int) (Math.random() * 100);
	}

	private static int getRandomY() {
		return (int) (Math.random() * 100);
	}
}
