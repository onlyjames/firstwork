package FlyweightPattern;

import java.util.HashMap;

public class ShapeFactory {
	private static final HashMap<String, Shape> circleMap = new HashMap();

	public static Shape getCircle(String color) {
		Circle circle = (Circle) circleMap.get(color);  //取出原本的物件
		if(circle==null){
		circle = new Circle(color);
		circleMap.put(color, circle);
		}                    
		System.out.println("建立一個圓形顏色為 : " + color);
		return circle;
	}
}
