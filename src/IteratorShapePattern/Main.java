package IteratorShapePattern;

import java.util.ArrayList;
import java.util.Iterator;

public class Main {
	public static void main(String[] args) {
		ShapeStorage storage = new ShapeStorage();
		storage.addShape("Polygon");
		storage.addShape("Hexagon");
		storage.addShape("Circle");
		storage.addShape("Rectangle");
		storage.addShape("Square");

		ShapeIterator iterator = new ShapeIterator(storage.getShapes());
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		System.out.println("Apply removing while iterating...");
		iterator = new ShapeIterator(storage.getShapes());
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
			iterator.remove();
		}

		System.out.println("==========ForEach===============");
		ArrayList<String> in = new ArrayList<String>();
		in.add("1");
		in.add("2");
		in.add("3");
		in.add("4");
		in.add("5");
		in.add("6");
		in.forEach((n) -> {
			System.out.println(n);
		});
		System.out.println("==========For Loop======================");
		for (int i = 0; i < in.size(); i++) {
			System.out.println(in.get(i));
		}
		System.out.println("==========Iterator===============");
		Iterator k = in.iterator();
		while (k.hasNext()) {
			System.out.println(k.next());
		}
		System.out.println("remove");
		k.remove();
		while (k.hasNext()) {
			System.out.println(k.next());
		}
		String[] otherSample = new String[] { "1", "2", "3", "4" };
		
		// ============================================================================================
		/*
		 * ArrayList<String> in = new ArrayList<String>(); in.add("Sheep");
		 * in.add("Leo"); in.add("James"); in.add("Tony"); in.add("Jason");
		 * in.add("Richar");
		 * 
		 * for (int i = 0; i < in.size(); i++) {
		 * System.out.println("ArrayList item " + in.get(i)); }
		 * 
		 * System.out.println("Use Iterator----------------------------------");
		 * Iterator iter = in.listIterator(3);
		 * 
		 * while (iter.hasNext()) { System.out.println(iter.next());
		 * iter.remove(); } System.out.println("Remove-----------------"); while
		 * (iter.hasNext()) { System.out.println(iter.next()); }
		 */
	}
}
