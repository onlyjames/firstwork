package InterpreterTree;

public class ExecuteException extends Exception {
    public ExecuteException(String msg) {
        super(msg);
    }
}
