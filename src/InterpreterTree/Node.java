package InterpreterTree;

public abstract class Node implements Executor {  //AbstractExpression
    public abstract void parse(Context context) throws ParseException;
}
