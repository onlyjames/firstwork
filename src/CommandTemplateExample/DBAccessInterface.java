package CommandTemplateExample;

public interface DBAccessInterface {
	public abstract Object getX();

	public abstract void saveX(String x);
}
