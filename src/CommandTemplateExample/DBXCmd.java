package CommandTemplateExample;


public abstract class DBXCmd {

	public DBXCmd() {
		hook1();
		hook2();
	}

	final Object execute() {
		return null;
	}

	public abstract void hook1();

	public abstract Object hook2();
}
