package CommandTemplateExample;

public class DBXAccess implements DBAccessInterface{
	private DBXCmd cmd = null;

	public Object getX() {
		cmd=new GetX();
		return cmd.execute();
	}

	public void saveX(String x) {
		cmd=new SaveX(x);
		cmd.execute();
	}
}
