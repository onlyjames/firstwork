package CommandTemplateExample;

public class DBMgr {
	DBAccessInterface dbaccess = new DBXAccess();

	public Object getX() {
		return dbaccess.getX();
	}

	public void saveX(String x) {
		dbaccess.saveX(x);
	}
}
