package InterpreterPattern;

public abstract class Expression {
	public void interpret(String str) {
		if (str.length() > 0) {
			String text = str.substring(1, str.length());
			Integer number = Integer.valueOf(text);
			excute(number);
		}
	}

	// 要解釋的內容
	protected abstract void excute(Integer number);  //hook
}
