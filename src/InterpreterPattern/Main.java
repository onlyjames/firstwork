package InterpreterPattern;

/*目的：定義一個語言與其文法，使用一個解譯器來表示這個語言的敘述

建議直接跳過，這個模式很少用也很難用*/
public class Main {
	public static void main(String[] args) {
		Expression ex;
		Context context = new Context();
		context.setText("A4461 B1341 A676 B1787");

		System.out.println("============解譯器模式測試============");
		System.out.println("待解譯內容：A4461 B1341 A676 B1787");

		System.out.println("---解譯結果---");
		// A則後面的數字*2，B則後面的數字/2
		for (String str : context.getText().split("\\s")) {
			// 不同的解譯器代表可以藉由實作更多Expression的字類別來擴充解譯器能力
			if (str.charAt(0) == 'A') {
				ex = new UpExpression();
			} else {
				ex = new DownExpression();
			}

			ex.interpret(str);
		}
	}
}
