package JunitTest;

public class JunitTesting {
	public int squre(int i) {
		return i * i;
	}

	public int countA(String word) {
		int count = 0;
		for (int i = 0; i < word.length(); i++) {
			if (word.charAt(i) == 'a' || word.charAt(i) == 'A') {
				count++;
			}
		}
		return count;
	}
}