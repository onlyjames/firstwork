package DecoratorPatternInterfaceAnimal;

public class weapons extends Decorator {
	weapons(Pet pet) {
		super(pet);
	}

	public String getDescription() {
		return pet.getDescription() + " weapons";
	}
}
