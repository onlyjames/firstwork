package DecoratorPatternInterfaceAnimal;

public class cloak extends Decorator {
	cloak(Pet pet) {
		super(pet);
	}

	public String getDescription() {
		return pet.getDescription() + " cloak";
	}
}
