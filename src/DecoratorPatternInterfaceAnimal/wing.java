package DecoratorPatternInterfaceAnimal;

public class wing extends Decorator {
	wing(Pet pet) {
		super(pet);
	}

	public String getDescription() {
		return pet.getDescription()+" wing";
	}
}