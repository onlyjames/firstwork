package DecoratorPatternInterfaceAnimal;
// Decorator 跟 Strategy Pattern有什麼不一樣
/*Decorator          Strategy
 *  A                   A     A
 *  |                   |     |
 *  D1                 S1     S2
 *  |                   |     |
 *  B                   B     B
 *  |
 *  D2
 *  |
 *  C
 * */
public class Main {
	public static void main(String[] argu) {
		Pet k = new wing(new cloak(new dog()));
		Pet k1 = new wing(new cloak(k));
		System.out.println(k.getDescription());
		System.out.println(k1.getDescription());
	}

}
