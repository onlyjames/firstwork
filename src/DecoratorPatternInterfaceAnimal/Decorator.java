package DecoratorPatternInterfaceAnimal;

public abstract class Decorator implements Pet {
	Pet pet;

	public Decorator(Pet in) {
		pet = in;
	}
}
