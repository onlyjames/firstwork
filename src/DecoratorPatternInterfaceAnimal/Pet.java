package DecoratorPatternInterfaceAnimal;

public interface Pet {
	public String getDescription();
}
