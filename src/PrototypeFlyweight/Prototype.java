package PrototypeFlyweight;

public interface Prototype {
	public Prototype clone();
}
