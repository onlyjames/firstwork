package PrototypeFlyweight;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PrototypeMgr prototypeMgr = new PrototypeMgr();
		Prototype p = prototypeMgr.get("ReadMember");
		if (p == null) {
		    p = new ReadMemberCommand().clone();
			prototypeMgr.add("ReadMember", p);
		}
		ReadMemberCommand readMember=(ReadMemberCommand)p.clone();
		readMember.setAccount("James");
		readMember.setTable(" memberdata");
		 
		SqlCmd sqlCmd=(SqlCmd)readMember;
		sqlCmd.execute();
		User user=(User)sqlCmd.getResult();
	}

}
