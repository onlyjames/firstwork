package PrototypeFlyweight;

public class User {
	// user's data and this class judge customer
	private String account;
	private String password;
	private String name;
	private String email;
	private String id;
	private String age;

	public void setAccount(String account) {
		this.account = account;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getAccount() {
		return account;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getID() {
		return id;
	}

	public String getAge() {
		return age;
	}

	public boolean verify(String password) {
		// System.out.println(password+" user "+this.password);
		return (this.password.equals(password)) ? true : false;
	}

}
