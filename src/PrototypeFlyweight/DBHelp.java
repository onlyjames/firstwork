package PrototypeFlyweight;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBHelp { // In here use Singleton,because it need only one to
						// connect to database.
	private static volatile DBHelp instance = null;

	private DBHelp() {
	}

	public static DBHelp getInstance() {
		if (instance == null) {
			synchronized (DBHelp.class) {
				if (instance == null) {
					instance = new DBHelp();
				}
			}
		}
		return instance;
	}

	public Connection connectDB() {   //connect to database 
		Connection conn = null;
		try {
	         Class.forName("org.sqlite.JDBC");
	         conn = DriverManager.getConnection("jdbc:sqlite:oomovie.db");
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
	      }
	      System.out.println("Opened database successfully");
		return conn;
	}
	/*
public static void main(String[] argu){
	new DBHelp().connectDB();
}*/
//close database for select data behavior
	public void closeDB(ResultSet rs, PreparedStatement pst, Connection conn) {
		try {
			rs.close();
			pst.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
//close database for save update delete data behavior
	public void closeDBBatch(PreparedStatement pst, Connection conn) {
		try {
			//pst.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
