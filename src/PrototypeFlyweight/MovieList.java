package PrototypeFlyweight;

import java.util.ArrayList;

public class MovieList { // this model store theater's movies.
	ArrayList<Movie> movies = new ArrayList<Movie>();

	public void addMovie(Movie movie) {
		movies.add(movie);
	}

	public Movie getMovieInsideList(String name) {
		// if this theater has this movie, this movie object will be return
		for (int i = 0; i < movies.size(); i++) {
			if (name.equals(movies.get(i).getName())) {
				return movies.get(i);
			}
		}
		return null;
	}

	public String[] getMovies() {
		// get this theater has movies in String array to display in view
		String[] ans = new String[movies.size()];
		for (int i = 0; i < movies.size(); i++) {
			ans[i] = movies.get(i).getName();
		}
		return ans;
	}
}
