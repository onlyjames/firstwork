package PrototypeFlyweight;
import java.sql.*;


public abstract class SqlCmd {
	protected DBHelp dbConnect = null;
	private Object object = null;
	Connection conn = null;
	Statement sta = null;
	ResultSet rs = null;// Read 在使用的屬性 select command will use this.
	PreparedStatement pst = null;

	SqlCmd() {
		dbConnect = DBHelp.getInstance();
	}

	public void setObject(Object ob) {
		object = ob;
	}

	public Object getResult() {
		return object;
	}

	final public boolean execute() {
		// System.out.println("Doing execute");
		openDB();
		queryDB();
		if (checkProcessResult()) { // judge processResult need to execute.
			object = processResult();
		}
		disConnect();
		return true;
	}

	public void openDB() {
		try {
			conn = dbConnect.connectDB();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// In different Command will close different thing,but it must be close.
	public abstract void disConnect(); // primitive

	public abstract void queryDB();// primitive

	public Object processResult() { // because insert update SQL command
									// will not get object
									// so design in hook
		return null;
	}

	public boolean checkProcessResult() { // if you don't need use processResult
											// override this false
		return true;
	}
}
