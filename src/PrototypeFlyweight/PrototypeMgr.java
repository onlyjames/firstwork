package PrototypeFlyweight;

import java.util.HashMap;

public class PrototypeMgr {
	HashMap<String, Prototype> allElements = new HashMap<String, Prototype>();

	public Prototype get(String k) {
		if (allElements.get(k) != null) {
			return allElements.get(k);
		}
		return null;
	}

	public void add(String key, Prototype prototype) {
		allElements.put(key, prototype);
	}

	public void remove(String key){
		allElements.remove(key);
	}
}
