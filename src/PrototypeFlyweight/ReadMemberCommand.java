package PrototypeFlyweight;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ReadMemberCommand extends SqlCmd implements Prototype, Serializable{
	String account = "";
	String table;

	// Read member data from database
	// use account to identity user data
	public ReadMemberCommand() {
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setTable(String table) {
		this.table = table;
	}

	// close db
	@Override
	public void disConnect() {
		// TODO Auto-generated method stub
		dbConnect.closeDB(rs, pst, conn);
	}

	// ���o Ū���� ResultSet
	// read db. Result is ResultSet
	@Override
	public void queryDB() {
		// TODO Auto-generated method stub
		try {
			String sql = "select * from " + table + " where account = '" + account + "'";
			// memberdata
			pst = conn.prepareStatement(sql);
			pst.execute();

			rs = pst.getResultSet();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// in here , Analyze ResultSet to get data and save in object
	@Override
	public Object processResult() {
		// TODO Auto-generated method stub
		User user = null;
		try {
			if (rs.next()) {
				user = new User();
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("customername"));
				user.setId(rs.getString("id"));
				user.setEmail(rs.getString("email"));
				user.setAge(rs.getString("age"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return user;
	}
	public Prototype clone(){
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(this);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return (Prototype) ois.readObject();
		} catch (IOException e) {
			return null;
		} catch (ClassNotFoundException e) {
			return null;
		}
	}
}
