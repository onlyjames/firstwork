package PrototypeFlyweight;

import java.util.ArrayList;
import java.util.HashMap;

public class Movie {
	//store movie data, in here ,you can get movie data.
	private String name = "";
	HashMap<String, ArrayList<String>> allDate = new HashMap<String, ArrayList<String>>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDate(String date, String session) {    //設定電影日期與場次
		ArrayList<String> sessions = allDate.get(date);
		if (sessions != null) {
			sessions.add(session);
		} else {
			sessions = new ArrayList<String>();
			sessions.add(session);
			allDate.put(date, sessions);
		}
	}

	public String[] getMovieDate() {   //取得電影日期
		String[] dates = new String[allDate.size()];
		int i = 0;
		for (String k : allDate.keySet()) {
			dates[i] = k;
			i++;
		}
		return dates;
	}

	public String[] getMovieSession(String date) {  //取得電影場次
		ArrayList<String> array = allDate.get(date);
		String[] ans = new String[array.size()];
		for (int i = 0; i < array.size(); i++) {
			ans[i] = array.get(i);
		}
		return ans;
	}
	/*
	public void testResult(){
		//System.out.println("pass1");
		allDate.forEach((k,v)->{
			System.out.println(" "+k);
			v.forEach((n)->{
				System.out.println(" "+n);
			});
		});
	}
	*/
}