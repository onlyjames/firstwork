package CompositePattern;

/*
 * iterator 無法表示樹狀的菜單 所以使用Composite表示
 * */
public class Main {
	public static void main(String args[]) {
		MenuComponent pancakeHouseMenu = new Menu("PANCAKE HOUSE MENU", "Breakfast");
		MenuComponent dinerMenu = new Menu("DINER MENU", "Lunch");
		MenuComponent cafeMenu = new Menu("CAFE MENU", "Diner");
		MenuComponent dessertMenu = new Menu("DESSERT MENU", "Dessert of course!");

		MenuComponent allMenus = new Menu("ALL MENUS", "All menus combination");
		allMenus.add(pancakeHouseMenu);
		allMenus.add(dinerMenu);
		allMenus.add(cafeMenu);

		dinerMenu.add(new MenuItem("Pasta", "Spaghetti with...", true, 3.89));
		dinerMenu.add(dessertMenu);

		dessertMenu.add(new MenuItem("Apple Pie", "Apple pie with a flakey crust, topped with...", true, 1.59));

		// allMenus.add(dinerMenu);
		// allMenus.add(dessertMenu);
		allMenus.print();
		/*
		 * MenuItem test = new MenuItem("Pasta", "Spaghetti with...", true,
		 * 3.89);
		 * 
		 * 
		 * test.add(dessertMenu);
		 */
	}
}