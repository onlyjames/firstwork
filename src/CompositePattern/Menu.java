package CompositePattern;

import java.util.ArrayList;
import java.util.Iterator;

public class Menu extends MenuComponent {
	private String name;
	private String description;
	private ArrayList<MenuComponent> menuComponents;

	public Menu(String name, String dest) {
		this.name = name;
		this.description = dest;
		menuComponents = new ArrayList();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void add(MenuComponent comp) {
		menuComponents.add(comp);
	}

	@Override
	public void remove(MenuComponent comp) {
		menuComponents.remove(comp);
	}

	@Override
	public MenuComponent getChild(int i) {
		return menuComponents.get(i);
	}

	@Override
	public void print() {
		System.out.print("\n" + getName() + ", " + getDescription() + "\n");
		System.out.println("=====================================");

		for (MenuComponent comp : menuComponents) {
			comp.print();
		}
	}
}