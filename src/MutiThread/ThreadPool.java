package MutiThread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadPool implements Runnable {
	private final LinkedBlockingQueue<Runnable> queue;//works
	private final List<Thread> threads;
	private boolean shutdown;

	public ThreadPool(int numberOfThreads) {
		queue = new LinkedBlockingQueue<>();
		threads = new ArrayList<>();
		// first open thread, prepare to work
		for (int i = 0; i < numberOfThreads; i++) {
			Thread thread = new Thread(this);
			thread.start();
			threads.add(thread);
		}
	}

	public void execute(Runnable task) throws InterruptedException {
		queue.put(task);
	}

	private Runnable consume() throws InterruptedException {
		return queue.take();
	}

	public void run() {
		try {
			while (!shutdown) {
				Runnable task = this.consume();
				task.run();
			}
		} catch (InterruptedException e) {
		}
		System.out.println(Thread.currentThread().getName() + " shutdown");
	}

	public void shutdown() {
		shutdown = true;

		threads.forEach((thread) -> {
			thread.interrupt();
		});
	}

	public static void main(String[] args) throws InterruptedException {
		ThreadPool threadPool = new ThreadPool(5);
		Random random = new Random();

		for (int i = 0; i < 10; i++) {
			int fi = i;
			threadPool.execute(() -> {
				try {
					Thread.sleep(random.nextInt(1000));
					System.out.printf("task %d complete\n", fi);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});
		}

		Thread.sleep(3000);
		threadPool.shutdown();
	}
}
