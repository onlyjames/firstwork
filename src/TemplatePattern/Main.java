package TemplatePattern;

public class Main {
	public static void main(String args[]) {
		CookieHookRobot cookieHookRobot = new CookieHookRobot("Cookie Robot");
		System.out.println(cookieHookRobot.getName() + ":");
		cookieHookRobot.go();
	}
	/*
	?? Give primitive and hook methods protected access
	?? These methods are intended to be called by a template
	method, and not directly by clients
	
	?? Declare primitive methods as abstract in the superclass
	?? Primitive methods must be implemented by subclasses
	
	primitive methods is abstract in Template method
	
	?? Declare hook methods as non-abstract
	?? Hook methods may optionally be overridden by subclasses
	
	
	?? Declare template methods as final
	?? This prevents a subclass from overriding the method and
	interfering with it?�s algorithm structure
	*/
}
