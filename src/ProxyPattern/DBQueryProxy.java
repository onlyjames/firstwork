package ProxyPattern;

public class DBQueryProxy implements IDBQuery{
	private DBQuery real=null;

	public String request(){
		if(real==null){
			//真正需要 才new
			real=new DBQuery();
		}
		//在多線程 這裡回傳一個 虛假類，  很像Future pattern
		return real.request();
	}

}

/*
 * 動態proxy是指在運行時，動態的new proxy
 * Thread-Per-Message Pattern 會產生多 thread來執行任務 但沒有先後順序 先執行完 先顯示
 */
