package ProxyPattern;

public interface IDBQuery {
	public String request();
}
