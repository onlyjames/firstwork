package ProxyPattern;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class CglibDBQueryInterceptor implements MethodInterceptor {
	IDBQuery real = null;

	public CglibDBQueryInterceptor() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object intercept(Object arg0, Method arg1, Object[] arg2, MethodProxy arg3) throws Throwable {
		// TODO Auto-generated method stub
		if (real == null) {
			real = new DBQuery();
		}
		return real.request();
	}

}
