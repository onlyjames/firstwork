package ProxyPattern;

/*目的：為一個物件提供代理物件
 * 要開啟我們的冒險者遊戲其實要花費一翻很大的功夫，如果在讀取的過程畫面跟國防布一樣完全沒有畫面，
 * 玩家會 懷疑遊戲是不是壞了，因此我們用一個代理類別，讓遊戲還沒讀取完成之前先跟玩家說，遊戲讀取中...*/
public class Main {
	public static void main(String args[]) {
		// TODO Auto-generated constructor stub
		IDBQuery q = new DBQueryProxy();
		System.out.println(q.request());
		System.out.println(q.request());
	}
}
