package ProxyPattern;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

//JDK�ʺAProxy
public class JdkDBQueryHandler implements InvocationHandler {
	IDBQuery real = null;

	public JdkDBQueryHandler() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (real == null) {
			real = new DBQuery();
		}
		return real.request();
	}

	public static IDBQuery creatJdkProxy() {
		IDBQuery jdkProxy = (IDBQuery) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(),
				new Class[] { IDBQuery.class }, new JdkDBQueryHandler());
		return jdkProxy;
	}

}
