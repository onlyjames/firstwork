package ClassLoader;
//this is not Design Pattern
public class Main {
	public void loadClass() throws Exception {
		ClassLoader current = getClass().getClassLoader();
		Class<?> clazz = current.loadClass("java.util.ArrayList");
		Object str = clazz.newInstance();
		System.out.println(str.getClass());
	}

	public void displayParents(){
		ClassLoader current=getClass().getClassLoader();
		while(current!=null){
			System.out.println(current.toString());
			current=current.getParent();
		}
	}
	
	public static void main(String[] argu) {
		Main m = new Main();
		try {
			m.loadClass();
			m.displayParents();
		} catch (Exception e) {
			System.out.println("Load Class Error");
		}
		m.displayParents();
	}
}
