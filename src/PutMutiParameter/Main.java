package PutMutiParameter;

public class Main {
	public void addNumber(int... parameter) {
		int sum = 0;
		for (int i = 0; i < parameter.length; i++) {
			System.out.print(parameter[i] + "  ");
		}
		System.out.println();
		for (int i : parameter) {
			sum += i;
		}
		System.out.println("Sum is " + sum);
	}

	public static void main(String[] argu) {
		Main mm = new Main();
		System.out.println("========Round 1=========");
		mm.addNumber(10, 20, 30, 40, 50, 60);
		System.out.println("=========Round 2========");
		mm.addNumber(10, 20, 30);
	}
}
