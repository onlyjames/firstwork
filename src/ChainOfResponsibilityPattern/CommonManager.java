package ChainOfResponsibilityPattern;

//經理
public class CommonManager extends Manager {
	public CommonManager(String name) {
		super(name);
	}

	@Override
	public void apply(ApplyRequest request) {
		// TODO Auto-generated method stub
		if (request.getRequestType().equals("請假") && request.getRequestCount() <= 2) {
			System.out.print(request.getRequestType() + "：" + request.getRequestContent());
			System.out.println(" " + request.getRequestCount() + "天 被" + name + "批准");
		} else {
			if (superior != null) {
				superior.apply(request);
			}
		}
	}

}
