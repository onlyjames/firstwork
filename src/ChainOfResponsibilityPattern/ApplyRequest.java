package ChainOfResponsibilityPattern;

//提出申請
public class ApplyRequest {
	// 申請類別
	private String requestType;
	// 申請內容
	private String requestContent;
	// 申請數量
	private int requestCount;

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String in) {
		requestType = in;
	}

	public String getRequestContent() {
		return requestContent;
	}

	public void setRequestContent(String in) {
		requestContent = in;
	}

	public int getRequestCount() {
		return requestCount;
	}

	public void setRequestCount(int in) {
		requestCount = in;
	}
}
