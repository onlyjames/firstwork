package ChainOfResponsibilityPattern;

//管理者
public abstract class Manager {
	protected String name;

	// 上司
	protected Manager superior;

	public Manager(String name) {
		this.name = name;
	}

	// 設定上司
	public void setSuperior(Manager superior) {
		this.superior = superior;
	}
	//提出申請
	public abstract void apply(ApplyRequest request);
}
