package FactoryPatternPizza;

public class NYPizzaStore extends PizzaStore {

	public NYPizzaStore() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Pizza createPizza(String type) {
		// TODO Auto-generated method stub
		if (type.equals("cheese")) {
			return new NYStyleCheesePizza();
		}
		return null;
	}

}
