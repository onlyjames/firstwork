package SingletonPattern_different_method;

public class VolatileExample {
	private static boolean running = false;
	private static Integer a = new Integer(10);

	public static void main(String[] argu) throws Exception {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (!running) {
				}
				System.out.println("Started.");
				while (running) {
				}
				System.out.println("Stopped.");
			}
		}).start();

		Thread.sleep(1000);
		System.out.println("Thread Start");
		running = true;

		Thread.sleep(1000);
		System.out.println("Thread Stop");
		running = false;

	}
}
