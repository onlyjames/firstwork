package SingletonPattern_different_method;

public class LazySingleton {
	private LazySingleton() {
		System.out.println("Creat LazeSingleton");
	}

	private static LazySingleton instance = null; //減少系統加載 的負擔

	public static synchronized LazySingleton getInstance() {//但要new時需要判斷是否有被new了 造成效能上的損失
		if (instance == null) {
			instance = new LazySingleton();
		}
		return instance;
	}
	public static void main(String[] argu){
		LazySingleton LS=LazySingleton.getInstance();
	}
}
