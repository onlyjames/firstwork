package SingletonPattern_different_method;

public class SerSingleton implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 對象通過寫出描述自己狀態的數值來記錄自己 ，這個過程叫對象的串行化(Serialization)
	String id;

	private SerSingleton() {
		System.out.println("Creat Singleton");
		// Creat的過程可能比較慢
		id = "SerSingleton";
	}

	private static SerSingleton instance = new SerSingleton();

	public static SerSingleton getInstence() {
		return instance;
	}

	public static void creatString() {
		System.out.println("CreatString in Singleton");
	}

	private Object readResolve() { // 阻止產生新instence 回傳現有instence 在read object時執行
		return instance;
	}
}
