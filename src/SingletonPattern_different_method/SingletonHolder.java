package SingletonPattern_different_method;

public class SingletonHolder {
	// 一開始 JVM 不會載入 inner class，而且 JVM 不屬於自己系統的 Thread 所以不會有 volatile 的問題

	private static class Holder {
		private static SingletonHolder instance = new SingletonHolder();
	}

	// 當要使用時 會去拿取 instance，這裡使用了 JVM(classLoader) 延遲加載的技巧
	// 如果沒有 instance JVM 會使用 classLoader 將 class 載入。
	public static SingletonHolder getInstance() {
		return Holder.instance;
	}
}
