package SingletonPattern_different_method;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainTest implements Runnable {
	// 例外的 creat 單例 以下方法不適用，因為可能會跑出1個以上的單例
	long begintime = System.currentTimeMillis();
	private ObjectInputStream ois;

	@Override
	public void run() {
		for (int i = 0; i < 1000000; i++) {
			// Singleton.getInstence();
			// LazySingleton.getInstance();
			StaticSingleton.getInstence();
			System.out.println("spend:" + (System.currentTimeMillis() - begintime));
		}
	}

	public void testSerSingle() throws Exception {
		SerSingleton s = SerSingleton.getInstence();
		SerSingleton s1 = null;
		s.id = "change";
		// 將instance 串行化到txt
		// 物件序列化簡單來說就是將物件變成資料流，可以將物件儲存成檔案，可以永久儲存在硬碟中，而且可以在2台電腦之間傳遞物件。
		FileOutputStream fos = new FileOutputStream("SerSingleton.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(s);
		oos.flush(); // 把內部buffer的資料寫入txt
		oos.close();
		// 從txt讀出 原有 single instance
		FileInputStream fis = new FileInputStream("SerSingleton.txt");
		ois = new ObjectInputStream(fis);
		s1 = (SerSingleton) ois.readObject(); // 讀取內容物件 物件反序列化就是將檔案還原成物件，並拿來使用。
		System.out.println(s1.id);

	}

	public static void main(String[] argu) throws Exception {
		// Thread t = new Thread(new MainTest()); // 產生Thread物件
		// t.start();
		// new MainTest().run();
		// Singleton.getInstence();
		// Singleton.creatString();
		// LazySingleton.getInstance();
		//MainTest t = new MainTest();
		//t.testSerSingle();
		StaticSingleton test=StaticSingleton.getInstence();
		
	}
}
