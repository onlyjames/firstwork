package SingletonPattern_different_method;

public class StaticSingleton {
	int id = 0;

	private StaticSingleton() {
		System.out.println("Creat StaticSingleton.");
	}

	public void gett() {
		System.out.println("dfs");
	}

	private static class SingletonHolder { // 載入JVM時 不會 new  android用
											// StaticSingleton(初始化單例類)
		// 呼叫getInstence才會載入，getInstence不需要使用 synchronized(同步) 故性能比Lazy好
		private final static StaticSingleton instence = new StaticSingleton();
		// static 表示記憶區塊 只會有一個 StaticSingleton的instence變數
	}

	public static StaticSingleton getInstence() {
		return SingletonHolder.instence;
	}
}
