package SingletonPattern_different_method;

public class Singleton {
	// public int k = 0;

	private Singleton() {
		System.out.println("Creat Singleton");
	}

	private static Singleton instance = new Singleton();

	public static Singleton getInstance() {
		return instance;
	}
	/*
	 * public static void creatString(){ System.out.println("CreatString"); }
	 */
}
