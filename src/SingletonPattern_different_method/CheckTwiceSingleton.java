package SingletonPattern_different_method;

public class CheckTwiceSingleton {
	private volatile static CheckTwiceSingleton Instance=null;

	// private static CheckTwiceSingleton Instance;
	// volatile 程度較輕的
	// synchronized";與
	// synchronized
	// 塊相比，volatile
	// 變數所需的編碼較少，並且運行時開銷也較少，但是它所能實現的功能也僅是
	// synchronized
	// 的一部分。
	/*
	 * 為何要在這Pattern的instance加上volatile關鍵字。
	 * 
	 * 為了讓Singleton instance初始化正常，不會初始化到一半就被別的thread取走了。
	 * 
	 * 確保編譯器不會幫你對Singleton的code進行優化，讓一切判斷如你預期般的執行。
	 * 
	 * 確保指令在執行時不會被重新排序，造成Singleton的值有異常。 值得注意的是，在JAVA
	 * 5以上才能用這種雙重鎖的技巧，原因volatile在JAVA 5之後變得比較完善(JSR-133實現)。。
	 */
	private CheckTwiceSingleton() {
		// TODO Auto-generated constructor stub
	}

	public static CheckTwiceSingleton getInstance() {
		if (Instance == null) { // 先判斷了
			synchronized (CheckTwiceSingleton.class) { // 效能比 直接用synchronized好
														// (通常synchronized
														// 只會在執行第一次時 有用到)
				if (Instance == null) {
					Instance = new CheckTwiceSingleton();
				}
			}
		}
		return Instance;
	}
}
