package JAVAReflection;

import java.lang.reflect.Field;

public class JavaReflection {
	public static void main(String[] argu)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Class hName = Class.forName("JAVAReflection.hello");
		Object h = hName.newInstance();
		((hello) h).sayHello();
		System.out.println(((hello) h).getName());
		try {
			Field name = hello.class.getDeclaredField("name");
			name.setAccessible(true);
			name.set(h, "hi");
			System.out.println(((hello) h).getName());
		} catch (NoSuchFieldException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*
	 * Java 在編譯的時候是 載入.java檔 將他編譯成 .class檔(二進制檔) 可以在程式執行的時候 將 .class檔載入 使用 JVM產生
	 * java.lang.Class的instance 代表該 檔案 從 class的instance開始，就可以獲得class的許多訊息
	 * 從Class等API取得類別的資訊稱為 reflection
	 */
}
