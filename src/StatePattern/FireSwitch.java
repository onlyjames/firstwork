package StatePattern;

public class FireSwitch {
    private State _current;
                                                                                
    public FireSwitch() {             //�@�}�l�����A
        _current = new OffState();
    }
                                                                                
    public void setState(State s) {
        _current = s;
    }
                                                                                
    public void switchFire() {
        _current.switchFire(this);
    }
    public void doingSomething(){
    	_current.handle();
    	
    }
}