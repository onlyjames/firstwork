package StatePattern;

/*目的：將物件的狀態封裝成類別，讓此物件隨著狀態改變時能有不同的行為
 * 很多事物的行為模式會隨著狀態而改變，例如說毛毛蟲只能在地上爬，之後變成蛹就入定不動了，破蛹而出變蝴蝶不但可以爬還可以飛，
 * 一般來說 我們會用if else或是switch case，不過根據書上的說法，有些人不喜歡看到一大堆if esle或switch case這種判斷式，
 * 因此就將會改變的狀態 封裝成類別，這樣可以減少一些判斷式，也可以讓責任由狀態類別分擔。*/
public class Main {
	public static void main(String[] args) {
		FireSwitch fireSwitch = new FireSwitch();
		fireSwitch.switchFire();
		fireSwitch.switchFire();
		fireSwitch.switchFire();
		fireSwitch.switchFire();
		
		
		
		fireSwitch.doingSomething();
	}
}